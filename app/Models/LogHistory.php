<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LogHistory extends Model
{
    use HasFactory;

    protected $table = 'tbl_log_history';


    public static function getSeverity($request)
    {
        $result = DB::table('tbl_log_history')
             ->select(DB::raw('severity, count(severity) as count'))
             ->where('sensor_id', $request['sensor_id'])
             ->whereBetween('date_time', [$request['start_date'], $request['end_date']])
             ->groupBy('severity')
             ->get();

        return $result;
    }

    public static function getSeriesData($request)
    {
        $result = DB::table('tbl_log_history')
             ->select(DB::raw('date_time, value'))
             ->where('sensor_id', $request['sensor_id'])
             ->whereBetween('date_time', [$request['start_date'], $request['end_date']])
             ->orderBy('date_time', 'desc')
             ->get();

        return $result;
    }
}
